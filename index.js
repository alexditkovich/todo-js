const tasksForm = document.getElementById('form');
const tasksInp = document.getElementById('inp');
const tasksBox = document.getElementById('tasksBox');

const SAVED_TASKS = 'SAVED_TASKS';

const tasksArray = localStorage.getItem(SAVED_TASKS) ? JSON.parse(localStorage.getItem(SAVED_TASKS)) : [];

function invertIsComplete (index) {
    tasksArray[index].isComplete = !tasksArray[index].isComplete;
    localStorage.setItem(SAVED_TASKS, JSON.stringify(tasksArray));
}

function renderTasks () {
    tasksBox.innerHTML = '';

    tasksArray.forEach(function (item, index) {
        const element = `
            <div>
                <input type="checkbox" ${item.isComplete ? 'checked' : ''} onclick="invertIsComplete(${index})" />
                ${item.title}
            </div>
        `;

        tasksBox.insertAdjacentHTML('beforeend', element);
    })
}

renderTasks();

let eventTask = '';

tasksInp.oninput = function (e) {
    eventTask = e.target.value;
}

tasksForm.onsubmit = function (e) {
    e.preventDefault();
    tasksArray.push({title: eventTask, isComplete: false });
    localStorage.setItem(SAVED_TASKS, JSON.stringify(tasksArray));
    renderTasks();
}
